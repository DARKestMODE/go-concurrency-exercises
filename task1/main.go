package main

import (
	"fmt"
	"time"
)

var done chan bool
var tw chan *Tweet

func producer(stream Stream) {
	for {
		tweet, err := stream.Next()
		if err == ErrEOF {
			close(tw)
			return
		}
		tw <- tweet
	}
}

func consumer() {
	for t := range tw {
		if t.IsTalkingAboutGo() {
			fmt.Println(t.Username, "\ttweets about golang")
		} else {
			fmt.Println(t.Username, "\tdoes not tweet about golang")
		}
	}
	done <- true
}

func main() {
	tw = make(chan *Tweet)
	done = make(chan bool)

	start := time.Now()
	stream := GetMockStream()

	go producer(stream)
	go consumer()
	<-done
	fmt.Printf("Process took %s\n", time.Since(start))
}